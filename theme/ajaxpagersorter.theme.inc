<?php
/**
 * Preprocessors for AJAX Pager Sorter module.
 */

/**
 * Add common javascript both pagers and sorters.
 */
function _ajaxpagersorter_add_js(&$vars) {

  $settings = array(
    'ajaxpagersorter' => array(
      'loadingClass' => $vars['loading_class'],
      'wrapperOuterSelector' => $vars['wrapper_outer_selector'],
      'wrapperInnerSelector' => $vars['wrapper_inner_selector'],
    ),
  );
  drupal_add_js($settings, 'setting');
  $path = drupal_get_path('module', 'ajaxpagersorter') . '/ajaxpagersorter.js';
  drupal_add_js($path, 'file');
}

/**
 * Implements hook_preprocess_ajaxpagersorter_pager().
 */
function ajaxpagersorter_preprocess_ajaxpagersorter_pager(&$vars) {

  _ajaxpagersorter_add_js($vars);
  $settings = array(
    'ajaxpagersorter' => array(
      'pagerButtonText' => $vars['pager_button_text'],
      'pagerButtonClass' => $vars['pager_button_classes'],
    ),
  );
  drupal_add_js($settings, 'setting');

  $vars['pager'] = theme('pager');
}

/**
 * Implements hook_preprocess_ajaxpagersorter_sorter().
 */
function ajaxpagersorter_preprocess_ajaxpagersorter_sorter(&$vars) {

  _ajaxpagersorter_add_js($vars);

  if (empty($vars['default_sort'])) {
    // Get the first sort button to use as the default.
    $default_sort = NULL;
    foreach ($vars['sort_buttons'] as $key => $label) {
      $default_sort = $key;
      break;
    }
  }
  else {
    $default_sort = $vars['default_sort'];
  }

  $settings = array(
    'ajaxpagersorter' => array(
      'sortButtonClass' => $vars['sort_button_classes'],
      'sortButtonActiveClass' => $vars['sort_button_active_class'],
      'sortCurrent' => $default_sort,
    ),
  );
  drupal_add_js($settings, 'setting');

  $links = array();
  foreach ($vars['sort_buttons'] as $key => $label) {
    $classes = $vars['sort_button_classes'];
    if ($key == $default_sort) {
      $classes[] = $vars['sort_button_active_class'];
    }
    $links[] = l($label, current_path(), array(
      'query' => array(
        'sort' => $key,
      ),
      'attributes' => array(
        'class' => $classes,
      ),
    ));
  }
  $vars['sort_button_links'] = $links;
}