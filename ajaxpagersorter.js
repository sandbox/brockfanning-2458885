(function ($) {

  Drupal.behaviors.busaEntityAjaxEFQPager = {

    attach: function (context, settings) {

      // Gather the required settings.
      var config = settings.ajaxpagersorter,
          outerSelector = config.wrapperOuterSelector,
          innerSelector = config.wrapperInnerSelector,
          loadingClass = config.loadingClass,
          rememberState = window.history && window.history.replaceState,
          doPager = false,
          doSorter = false,
          pagerWrapperSelector = outerSelector + ' .item-list',
          pagerSelector = pagerWrapperSelector + ' .pager',
          $pagerButton,
          sortButtonSelector,
          currentSort,
          doNotCreatePagerButton = false,
          assignLinkMemory = function(selector, lastPage) {
            if (rememberState) {
              var html = $(selector).html();
              $(selector + ' a').once('ajaxpagersorter').click(function (e) {
                var stateObj = {
                  ajaxpagersorter: {
                    content: html,
                    selector: selector,
                    lastPage: lastPage,
                    currentSort: currentSort
                  }
                };
                history.replaceState(stateObj);
              });
            }
          },
          updateSortButtons = function() {
            $(sortButtonSelector).removeClass(config.sortButtonActiveClass);
            $(outerSelector + ' a' + sortButtonSelector + '[href*="?sort=' + currentSort + '"]').addClass(config.sortButtonActiveClass);
          };

      // Are we doing a pager?
      if (typeof config.pagerButtonText !== 'undefined') {

        if ($(pagerWrapperSelector, context).length) {
          doPager = true;
        }
      }

      // Are we doing a sorter?
      if (typeof config.sortButtonClass !== 'undefined') {

        sortButtonSelector = '.' + config.sortButtonClass.join('.');
        if ($(sortButtonSelector).length) {
          doSorter = true;
          currentSort = config.sortCurrent;
        }
      }

      // If the back button was pressed, start by remembering markup.
      if (rememberState && history.state && typeof history.state.ajaxpagersorter !== 'undefined') {

        $(history.state.ajaxpagersorter.selector).html(history.state.ajaxpagersorter.content);

        // If the user was already on the last page, tell this script not to
        // create a pager button.
        if (doPager && history.state.ajaxpagersorter.lastPage) {

          doNotCreatePagerButton = true;
        }

        // If the user was sorting, remember that state.
        if (doSorter && history.state.ajaxpagersorter.currentSort) {

          if (currentSort != history.state.ajaxpagersorter.currentSort) {

            currentSort = history.state.ajaxpagersorter.currentSort;
            updateSortButtons();
          }
        }

        // Don't want to do this twice.
        var stateClone = history.state;
        delete stateClone.ajaxpagersorter;
        history.replaceState(stateClone);
      }

      if (doPager) {

        $pagerButton = $('.' + config.pagerButtonClass.join('.'));
        // Create the button if needed.
        if (!$pagerButton.length && !doNotCreatePagerButton) {

          $pagerButton = $('<button/>', {
            text: config.pagerButtonText,
            class: config.pagerButtonClass.join(' ')
          });
          $pagerButton.insertAfter($(pagerWrapperSelector));
          // Click functionality for "load more" button.
          $pagerButton.click(function (e) {

            var nextPage = $(pagerSelector + ' .pager-next a').attr('href'),
              lastPage = $(pagerSelector + ' .pager-last a').attr('href'),
              $self = $(this),
              ajaxTarget = nextPage;

            if ($self.hasClass(loadingClass)) {

              return;
            }

            $self.addClass(loadingClass);

            // Do ajax call for the next page.
            $.get(ajaxTarget, function (data) {

              // Add the new content into the inner wrapper.
              $(data).find(innerSelector).children().appendTo($(innerSelector));

              // Remove the show-more button on the last page.
              if (nextPage == lastPage) {

                $self.hide();
                // Assign link-memory for back-button support, indicating that
                // this was the last page.
                assignLinkMemory(outerSelector + ' ' + innerSelector, true);
              }
              // Otherwise grab the pager from the next page as well.
              else {

                $(pagerSelector).replaceWith($(data).find(pagerSelector));
                // And hide it again.
                $(outerSelector + ' h2.element-invisible').remove();
                $(pagerWrapperSelector).hide();
                // Assign link-memory for back-button support.
                assignLinkMemory(outerSelector + ' ' + innerSelector);
              }

              $self.removeClass(loadingClass);
              Drupal.attachBehaviors($(outerSelector).parent().get(0));
            });
          });
        }

        // Always hide the pager.
        $(pagerWrapperSelector).hide();
        $(outerSelector + ' h2.element-invisible').remove();
      }

      if (doSorter) {

        // Assign behavior to the sort buttons.
        $(sortButtonSelector, context).once('ajaxpagersorter').click(function (e) {

          e.preventDefault();

          var $self = $(this),
              href = $self.attr('href'),
              urlParts = href.split('?'),
              queryString = urlParts[1].split('&'),
              keyVal,
              sortVal = false,
              ajaxTarget;

          for (var i in queryString) {
            keyVal = queryString[i].split('=');
            if (keyVal[0] == 'sort') {
              sortVal = keyVal[1];
              break;
            }
          }

          if (!sortVal ||
              $self.hasClass(loadingClass) ||
              sortVal == currentSort) {

            return;
          }

          doNotCreatePagerButton = false;

          $self.addClass(loadingClass);
          ajaxTarget = urlParts[0] + '?sort=' + sortVal;

          $.get(ajaxTarget, function (data) {

            if ($(data).find(innerSelector).length) {

              // Update the current sort.
              currentSort = sortVal;

              updateSortButtons();

              // Completely replace the contents of the ajax container.
              $(innerSelector).replaceWith($(data).find(innerSelector));

              // If there is a pager, we need to do some things.
              if (doPager && $(data).find(pagerSelector).length) {
                $pagerButton.show();
                $(pagerSelector).replaceWith($(data).find(pagerSelector));
                $(outerSelector + ' h2.element-invisible').remove();
                $(pagerWrapperSelector).hide();
                // Assign link-memory for back-button support.
                assignLinkMemory(outerSelector + ' ' + innerSelector);
              }
              else if (doPager) {
                $pagerButton.hide();
                // Assign link-memory for back-button support, indicating that
                // this was the last page.
                assignLinkMemory(outerSelector + ' ' + innerSelector, true);
              }

              $self.removeClass(loadingClass);
              Drupal.attachBehaviors($(outerSelector).parent().get(0));
            }
            else {
              $self.removeClass(loadingClass);
              alert('We regret that this option is not available at the moment.');
            }
          });
        });
      }
    }
  };
})(jQuery);
